const moment = require('moment');
const day = moment().date();
const puzzle = process.argv[2] || 1;

const { runPuzzle } = require(`./day${day}/day${day}p${puzzle}`);

runPuzzle();
