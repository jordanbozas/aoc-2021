const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const input = await processLineByLine('./day4/day4_input');
    const winningNumbers = Array.from(input[0]);
    let set = 0;
    let boards = _.chunk(input.slice(2, input.length).reduce((boards, line) => {
      if (line !== '') {
        line = _.compact(line.split(' ')).map(Number);
        boards.push(line);
      }
      return boards;
    }, []), 5);
    let sum = 0;
    let winningBoard = [];
    let winningNumber = -1;
    winningNumbers.forEach(number => {
      boards = boards.map(board => {
        board = board.map(line => {
          line = line.map(entry => entry === Number(number) ? 0 : entry)
          console.log(line)
          if (_.sum(line) === 0) {
            winningNumber = number;
          }
          return line;
        })
        // console.log(board);
        if (anyColumnsZero(board)) {
          winningBoard = board;
        }
        return board;
      })
    });
    console.log(winningNumber)
    console.log(winningBoard);
    // answer = 2743844
}

function anyColumnsZero(board) {
  let zeros = false;
  const columns = board[0].length;
  for (let i = 0; i < columns; i++) {
    const column = board.map(x => x[i]);
    // console.log(column);
    // console.log(_.sum(column));
    if (_.sum(column) === 0) {
      zeros = true;
    }
  }
  return zeros;
}

module.exports = { runPuzzle };