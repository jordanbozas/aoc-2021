const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const input = await processLineByLine('./day3/day3_input');
    const binaryDiagnostic = input.map(line => Array.from(line));
    const transposed = binaryDiagnostic[0].map((x,i) => binaryDiagnostic.map(x => x[i]));
    const result = _.reduce(transposed, (previous, current) => {
        const count = _.countBy(current, Number);
        const zeros = count[0];
        const ones = count[1];
        if (zeros > ones) {
            previous.gamma.push(0);
            previous.epsilon.push(1);
        } else {
            previous.gamma.push(1);
            previous.epsilon.push(0);
        }
        return previous;
    }, { gamma: [], epsilon: [] });
    const gamma = result.gamma.join('');
    const epsilon = result.epsilon.join('')
    const gammaDecimal = parseInt(gamma, 2);
    const epsilonDecimal = parseInt(epsilon, 2);
    console.log(gammaDecimal * epsilonDecimal);
    // answer = 2743844
}

module.exports = { runPuzzle };