const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const input = await processLineByLine('./day3/day3_input');
    const binaryDiagnostic = input.map(line => Array.from(line));
    const columnsLength = binaryDiagnostic[0].length;

    let oxygenRating = binaryDiagnostic;
    let co2Rating = binaryDiagnostic;

    for (let i=0; i<columnsLength; i++) {
        if (oxygenRating.length === 1) {
            break;
        }
        let column = [];
        for (let j=0; j<oxygenRating.length; j++) {
            column.push(oxygenRating[j][i]);
        }
        const count = _.countBy(column, Number);
        const zeros = count[0];
        const ones = count[1];
        if (zeros > ones) {
            oxygenRating = _.filter(oxygenRating, line => Number(line[i]) === 0);
        } else {
            oxygenRating = _.filter(oxygenRating, line => Number(line[i]) === 1);
        }
    }

    for (let i=0; i<columnsLength; i++) {
        if (co2Rating.length === 1) {
            break;
        }
        let column = [];
        for (let j=0; j<co2Rating.length; j++) {
            column.push(co2Rating[j][i]);
        }
        const count = _.countBy(column, Number);
        const zeros = count[0];
        const ones = count[1];
        if (zeros > ones) {
            co2Rating = _.filter(co2Rating, line => Number(line[i]) === 1);
        } else {
            co2Rating = _.filter(co2Rating, line => Number(line[i]) === 0);
        }
    }

    const oxygenGeneratorRating = parseInt(oxygenRating[0].join(''), 2);
    const co2ScrubberRating = parseInt(co2Rating[0].join(''), 2);

    console.log(oxygenGeneratorRating * co2ScrubberRating);
    // answer = 6677951
}

module.exports = { runPuzzle };