const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const depthMeasurements = await processLineByLine('./day1/day1_input');
    let deeper = 0;
    _.reduce(depthMeasurements, (previous, current) => {
        if (Number(previous) < Number(current)) {
            deeper++;
        }
        return current;
    });

    console.log(deeper);
    // answer = 1722
}

module.exports = { runPuzzle };