const { processLineByLine, partitionAndSum } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const depthMeasurements = await processLineByLine('./day1/day1_input');
    const partitioned = partitionAndSum(3, 1, depthMeasurements);
    let deeper = 0;
    _.reduce(partitioned, (prevGroup, currentGroup) => {
        if (prevGroup < currentGroup) {
            deeper++;
        }
        return currentGroup;
    });

    console.log(deeper);
    // answer = 1748
}

module.exports = { runPuzzle };