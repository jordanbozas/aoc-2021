const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const navigationInstructions = await processLineByLine('./day2/day2_input');
    let horizontalPosition = 0;
    let depth = 0;
    navigationInstructions.forEach(line => {
        const instructions = line.split(' ');
        const action = instructions[0];
        const step = Number(instructions[1]);
        switch (action) {
            case 'forward':
                horizontalPosition += step;
                break;
            case 'down':
                depth += step;
                break;
            case 'up':
                depth -= step;
                break;
            default:
                break;
        }
    })

    const result = horizontalPosition * depth;
    console.log(result);
    // answer = 2102357
}

module.exports = { runPuzzle };