const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const navigationInstructions = await processLineByLine('./day2/day2_input');
    let horizontalPosition = 0;
    let depth = 0;
    let aim = 0;
    navigationInstructions.forEach(line => {
        const instructions = line.split(' ');
        const action = instructions[0];
        const step = Number(instructions[1]);
        switch (action) {
            case 'forward':
                horizontalPosition += step;
                depth += aim * step; 
                break;
            case 'down':
                aim += step;
                break;
            case 'up':
                aim -= step;
                break;
            default:
                break;
        }
    })

    const result = horizontalPosition * depth;
    console.log(result);
    // answer = 2101031224
}

module.exports = { runPuzzle };