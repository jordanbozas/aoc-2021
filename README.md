# aoc-2021

My approach to Advent of Code 2021 using JS to solve the puzzles.

[Leaderboard](https://adventofcode.com/2021/leaderboard/private/view/515335)

## Usage

```bash
node aoc.js 1
```

## Contributing
Pull requests are welcome. Hack away.
