const { once } = require('events');
const { createReadStream, readFileSync } = require('fs');
const { createInterface } = require('readline');
const _ = require('lodash');

function process(fileName) {
  return readFileSync(fileName, "utf-8");
}

async function processLineByLine(fileName) {
  try {
    const rl = createInterface({
      input: createReadStream(fileName),
      crlfDelay: Infinity
    });
    const map = [];

    rl.on('line', (line) => {
      map.push(String(line));
    });

    await once(rl, 'close');

    return map;
  } catch (err) {
    console.error(err);
  }
}

function partitionAndSum ( size, step, collection ) {
  const result = [];
  for (let index = 0; index < collection.length; index += step) {
    const group = collection.slice(index, index + size);
    if (group.length === size) {
        const groupSum = _.sumBy(group, Number);
        result.push(groupSum);
    }
  }
  return result;
}

module.exports = { process, processLineByLine, partitionAndSum };
